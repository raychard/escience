import ColoredPencil from "./coloredPencil.js"
import GraphitePencil from "./graphitePencil.js"
import Pencil from "./pencilApp.js"

//Test input for Pencil
const write = new Pencil("teste")
console.log(write)
console.log(write.output())

//Test input for Graphite
const graphite = new GraphitePencil("Hello from Graphite Pencil")
console.log(graphite)
console.log(graphite.output())

//Test input for ColoredPencil for RED
const red = new ColoredPencil("Hello to RED")
console.log(red.red())
console.log(red.output())

const red1 = new ColoredPencil("Hello to RED AGAIN")
console.log(red1.red())
console.log(red1.output())

const red2 = new ColoredPencil("Hello to I AM THIRD RED")
console.log(red2.red())
console.log(red2.output())

//Test input for ColoredPencil for RED METHOD
console.log(red.red())
console.log(red1.red())
console.log(red2.red())

//Test input for ColoredPencil for BLUE
const blue = new ColoredPencil("Hello to BLUE")
console.log(blue.blue())
console.log(blue.output())

//Test input for ColoredPencil for GREEN
const green = new ColoredPencil("Hello to GREEN")
console.log(green.green())
console.log(green.output())
