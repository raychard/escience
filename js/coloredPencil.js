import Pencil from "./pencilApp.js";

export default class ColoredPencil extends Pencil{
    constructor(text){
        super(text)
    }

    write(){
        max.length = 30;
    }

    red(){
        return `RED Pen Selected: ${this.text}`
    }

    blue(){
        return `BLUE Pen Selected: ${this.text}`
    }

    green(){
        return `GREEN Pen Selected: ${this.text}`
    }
}