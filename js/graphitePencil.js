import Pencil from "./pencilApp.js";

export default class GraphitePencil extends Pencil{
    constructor(text){
        super(text)
    }
    
    write(){
        max.length = 50;                  
    }

    graphite(){
        return `Graphite Pen Selected ${this.text}`
    }
}